package main

import (
	"os"
	"net"
	"log"
	"errors"
	"io/ioutil"
	"gopkg.in/yaml.v2"
	"regexp"
	"strings"
)

type RewriteRule struct {
	Text string
	Status int
	Last bool
	Template string
	Pattern *regexp.Regexp
}

type GPConfig struct {
	RootDir string		  `yaml:"RootDir"`
	DBDir string		  `yaml:"DBDir"`
	TemplateDir string	  `yaml:"TemplateDir"`
	StaticDir string	  `yaml:"StaticDir"`
	ErrorDir string           `yaml:"ErrorDir"`
	Address string		  `yaml:"Address"`
	ProjectDirURI string      `yaml:"ProjectDirURI"`
	ProjectListURI string	  `yaml:"ProjectListURI"`
	CategoryListURI string	  `yaml:"CategoryListURI"`
	TrustedIP []string	  `yaml:"TrustedIP"`
	AccessLog string          `yaml:"AccessLog"`
	LogFormat string          `yaml:"LogFormat"`
	RewriteRules []string     `yaml:"RewriteRules"`
	RewriteDebug bool         `yaml:"RewriteDebug"`
	trustedNet []*net.IPNet
	rewriteRules []RewriteRule
}

func (cfg *GPConfig) compileTrustedNets() {
	cfg.trustedNet = []*net.IPNet{}
	for _, ips := range cfg.TrustedIP {
		_, cidr, err := net.ParseCIDR(ips)
		if err != nil {
			if ip := net.ParseIP(ips); ip != nil {
				_, bits := ip.DefaultMask().Size()
				cidr = &net.IPNet{
					IP: ip,
					Mask: net.CIDRMask(bits, bits)}
			} else {
				log.Printf("can't parse %s: %v", ips, err)
				continue
			}
		}
		cfg.trustedNet = append(cfg.trustedNet, cidr)
	}
}

func (cfg *GPConfig) IsTrustedIP(ips string) bool {
	if len(cfg.TrustedIP) > 0 {
		if cfg.trustedNet == nil {
//			log.Printf("compiling")
			cfg.compileTrustedNets()
		}
		ip := net.ParseIP(ips)
		if ip != nil {
			for _, ipnet := range cfg.trustedNet {
				if ipnet.Contains(ip) {
					return true
				}
			}
		}
	}
	return false
}

func (cfg *GPConfig) compileRewriteRules() {
	for i, s := range cfg.RewriteRules {
		f := strings.Fields(s)
		r := RewriteRule{Text: s}
		switch len(f) {
		case 3:
			for _, flag := range strings.Split(f[2], ",") {
				switch flag {
				case `301`, `permanent`:
					r.Status = 301
				case `302`,`temporary`:
					r.Status = 302
				case `last`:
					r.Last = true
				default:
					log.Printf("Unrecognized flag: %s", flag)
				}
			}
			fallthrough
		case 2:
			var err error
			r.Pattern, err = regexp.Compile(f[0])
			if err != nil {
				log.Printf("config: redirect %d: %v", i, err)
				continue
			}
			r.Template = f[1]
		default:
			log.Printf("config: redirect %d (%s): can't parse", i, s)
			continue
		}
		cfg.rewriteRules = append(cfg.rewriteRules, r)
	}
}

func (cfg *GPConfig) RewritePath(path string) (newpath string, status int, found bool) {
	for i, rule := range cfg.rewriteRules {
		if cfg.RewriteDebug {
			log.Printf("Rewrite: trying rule %d: %s", i, rule.Text)
		}
		submatches := rule.Pattern.FindStringSubmatchIndex(path)
		if submatches != nil {
			found = true
			newpath = string(rule.Pattern.ExpandString([]byte{}, rule.Template, path, submatches))
			status = rule.Status
			if cfg.RewriteDebug {
				log.Printf("Rewrite: rule %d: pattern matches, new path: %s", i, newpath)
			}

			if status != 0 {
				// Redirection rule returns immediately
				if cfg.RewriteDebug {
					log.Printf("Rewrite: rule %d: return status %d", i, status )
				}
				return
			}

			path = newpath
			if rule.Last {
				if cfg.RewriteDebug {
					log.Printf("Rewrite: rule %d: last", i)
				}
				return
			}
		}
	}
	return
}

var Config = GPConfig{
	RootDir: `.`,
	DBDir: `db`,
	TemplateDir: `tmpl`,
	StaticDir: `static`,
	ErrorDir: `error`,
	Address: `:8080`,
	ProjectDirURI: `/dir`,
	ProjectListURI: `/project`,
	CategoryListURI: `/category`,
	TrustedIP: []string{ "127.0.0.1" ,},
	LogFormat: `%a %l %u %t "%r" %>s %b "%{Referer}i" "%{User-Agent}i"`,
}

func ReadConfig() {
	config_file_name := `gp.conf`
	env_name := os.Getenv("GP_CONFIG")
	if env_name != "" {
		config_file_name = env_name
	}
	content, err := ioutil.ReadFile(config_file_name)
	if err != nil {
		if env_name == "" && errors.Is(err, os.ErrNotExist) {
			return
		} else {
			log.Panic(err)
		}
	}
	err = yaml.Unmarshal([]byte(content), &Config)
	if err != nil {
		log.Fatalf("%s: %v", config_file_name, err)
	}
	Config.compileTrustedNets()
	Config.compileRewriteRules()
}
