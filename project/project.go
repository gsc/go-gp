package project

import (
	"gp/object"
	"github.com/araddon/dateparse"
	"time"
	"sort"
)

type Object struct {
	object.Base `yaml:"-"`
	NewsURL string `yaml:"NewsURL" rem:"URL of the NEWS or other release log file."`
	Repository string `yaml:"Repository" rem:"URL of the project repository."`
	Description string `yaml:"Description" rem:"Short project description."`
	StartDate string `yaml:"StartDate" rem:"Date when the project was first registered in GP."`
	ModifiedDate string `yaml:"ModifiedDate" rem:"Most recent release date. Updated by gp rlog."`
	News string `yaml:"News" rem:"Noteworthy changes in the recent release. Updated by gp rlog."`
	NewsFormat string `yaml:"NewsFormat" rem:"Format of the release log file.  Possible values: GNU, CPAN, Python."`
	Url string `yaml:"Url" rem:"Home page URL."`
	Version string `yaml:"Version" rem:"Version of the recent release. Updated by gp rlog."`
	Category []string `yaml:"Category" rem:"List of the software categories this project belongs to."`
	Related []string `yaml:"Related" rem:"List of the names of other projects that are related to this one."`
}

func (proj *Object) Init() {
	proj.StartDate = time.Now().Format("2006-01-02")
}

func (proj *Object) Time() (t time.Time, err error) {
	return dateparse.ParseAny(proj.ModifiedDate)
}

func (proj *Object) Normalize() {
	sort.Strings(proj.Related)
	sort.Strings(proj.Category)
}
