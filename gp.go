package main

import (
	"os"
	"os/signal"
	"syscall"
	"log"
	"github.com/graygnuorg/go-gdbm"
	"gp/object"
	"gp/project"
	"gp/releaselog"
	"fmt"
	"path/filepath"
	"text/template"
	"regexp"
	"errors"
	"github.com/pborman/getopt/v2"
	"strings"
	"io/ioutil"
	"encoding/json"
	"sort"
	"net/http"
	"os/exec"
	"gopkg.in/yaml.v2"
	"bufio"
	_ "gp/releaselog/gnu"
	_ "gp/releaselog/cpan"
	_ "gp/releaselog/python"
)

var (
	Version = "1.2"
)

func VersionAction([]string) {
	fmt.Println("gp version", Version)
	fmt.Println(gdbm.VersionString())
}

type GPOptSet struct {
	*getopt.Set
	InitArgs []string
	hflag bool
}

func NewGPOptSet(args []string) (optset *GPOptSet) {
	optset = new(GPOptSet)
	optset.Set = getopt.New()
	optset.InitArgs = args
	optset.SetProgram(filepath.Base(os.Args[0]) + " " + args[0])
	optset.FlagLong(&optset.hflag, "help", 'h', "Show this help")
	return
}

func (optset *GPOptSet) Parse() {
	if err := optset.Getopt(optset.InitArgs, nil); err != nil {
		fmt.Fprintln(os.Stderr, err)
		optset.PrintUsage(os.Stderr)
		os.Exit(1)
	}

	if optset.hflag {
		optset.PrintUsage(os.Stdout)
		os.Exit(0)
	}
}

type DBOptSet struct {
	*GPOptSet
	datatype int
	cflag bool
	pflag bool
}

func NewDBOptSet(args []string) (optset *DBOptSet) {
	optset = new(DBOptSet)
	optset.init(args)
	return
}

func (optset *DBOptSet) init(args []string) {
	optset.GPOptSet = NewGPOptSet(args)
	optset.datatype = ProjectsType
	optset.SetProgram(filepath.Base(os.Args[0]) + " " + args[0])
	optset.FlagLong(&optset.cflag, "category", 'c', "Operate on categories");
	optset.FlagLong(&optset.pflag, "project", 'p', "Operate on projects (default)");
	return
}

func (optset *DBOptSet) Parse() {
	optset.GPOptSet.Parse()

	if optset.pflag {
		if optset.cflag {
			log.Fatalln("-c and -p can't be used together")
		}
		optset.datatype = ProjectsType
	} else if optset.cflag {
		optset.datatype = CategoryType
	}
}

type FormDBOptSet struct {
	DBOptSet
	Format string
}

func NewFormDBOptSet(args []string) (optset *FormDBOptSet) {
	optset = new(FormDBOptSet)
	optset.init(args)
	optset.FlagLong(&optset.Format, "format", 'F', "Output format")
	optset.Format = "{{ name . }} {{ json . }}"
	return
}

func (optset *FormDBOptSet) Parse() {
	optset.DBOptSet.Parse()
	if strings.HasPrefix(optset.Format, "@") {
		formfile := optset.Format[1:]
		content, err := ioutil.ReadFile(formfile)
		if err != nil {
			log.Fatalf("%s: %v", formfile, err)
		}
		optset.Format = string(content)
	}
}

func ListAction(args []string) {
	optset := NewFormDBOptSet(args)
	optset.SetParameters("[KEY]")
	optset.Parse()

	args = optset.Args()

	var key string

	switch len(args) {
	case 0:
		// Nothing
	case 1:
		key = args[0]

	default:
		log.Fatalln("bad arguments")
	}

	db, err := OpenDatabase(gdbm.ModeReader)
	if err != nil {
		log.Fatalf("Can't open database: %v", err)
	}
	defer db.Close()

	t := template.Must(object.Template().Parse(optset.Format))

	if key != "" {
		obj, err := db.FetchObject(optset.datatype, key)
		if err != nil {
			log.Fatalf("%s: %v", key, err)
		}
		err = t.Execute(os.Stdout, obj)
		if err != nil {
			log.Fatalf("executing template: %v", err)
		}
		fmt.Println("")
	} else {
		err := db.List(optset.datatype, t)
		if err != nil {
			log.Fatalf("List: %v", err)
		}
	}

}

func LoadAction(args []string) {
	nflag := false
	optset := NewGPOptSet(args)
	optset.FlagLong(&nflag, "purge", 'P', "remove all existing records from the database")
	optset.SetParameters("[FILE...]")
	optset.Parse()

	mode := gdbm.ModeWrcreat
	if nflag {
		mode = gdbm.ModeNewdb
	}
	db, err := OpenDatabase(mode)
	if err != nil {
		log.Fatalf("Can't open database: %v", err)
	}
	defer db.Close()

	args = optset.Args()
	if len(args) == 0 {
		args = append(args, "-")
	}
	for _, filename := range args {
		var file *os.File
		if filename == "-" {
			file = os.Stdin
		} else {
			file, err = os.Open(filename)
			if err != nil {
				log.Fatalf("%s: %v", filename, err)
			}
		}
		err = db.Load(file)
		if err != nil {
			log.Fatal(err)
		}
		file.Close()
	}
}

func DumpAction(args []string) {
	optset := NewGPOptSet(args)
	outfile := "-"
	optset.FlagLong(&outfile, "output", 'o', "Set output file name")
	optset.SetParameters("")
	optset.Parse()

	if len(optset.Args()) != 0 {
		log.Fatalln("bad arguments")
	}

	var file *os.File
	var err error
	if outfile != "-" {
		file, err = os.OpenFile(outfile, os.O_RDWR|os.O_CREATE, 0666)
		if err != nil {
			log.Fatalf("Can't open file %s: %v", outfile, err)
		}
	} else {
		file = os.Stdout
	}

	db, err := OpenDatabase(gdbm.ModeReader)
	if err != nil {
		log.Fatalf("Can't open database: %v", err)
	}
	defer db.Close()

	err = db.Dump(file)
	if err != nil {
		log.Fatalf("Dump failed: %v", err)
	}
}

func DeleteAction(args []string) {
	optset := NewDBOptSet(args)
	optset.SetParameters("KEY")
	optset.Parse()

	args = optset.Args()
	if len(args) != 1 {
		log.Fatalln("bad arguments")
	}

	db, err := OpenDatabase(gdbm.ModeWriter)
	if err != nil {
		log.Fatalf("Can't open database: %v", err)
	}
	defer db.Close()

	err = db.DeleteObject(optset.datatype, args[0])
	if err != nil {
		log.Fatalln(err);
	}
}

var rx = regexp.MustCompile(`^(["{[]).*([]"}])$`)
var match_delim = map[string]string{
	`"`:`"`,
	`[`:`]`,
	`{`:`}`,
}

func updateFromArgs(args []string) []byte {
	update := make(map[string]json.RawMessage)
	for _, arg := range args {
		kv := strings.SplitN(arg, ":", 2)
		if len(kv) != 2 {
			log.Fatalf("unrecognized argument: %s", arg)
			os.Exit(1)
		}
		m := rx.FindStringSubmatch(kv[1])
		if m == nil || match_delim[m[1]] != m[2] {
			kv[1] = `"` + strings.Replace(kv[1], `"`, `\"`, -1) + `"`
		}
		update[kv[0]] = json.RawMessage(kv[1])
	}
	jv, err := json.Marshal(update)
	if err != nil {
		log.Fatalln(err)
	}
	return jv
}

func updateFromFile(input string) []byte {
	content, err := ioutil.ReadFile(input)
	if err != nil {
		log.Fatalf("%s: %v", input, err)
	}
	return content
}

func UpdateAction(args []string) {
	input := ""
	optset := NewDBOptSet(args)
	optset.FlagLong(&input, "input", 'i', "Input file name")
	optset.SetParameters("KEY [ATTR:VALUE...]")
	optset.Parse()

	db, err := OpenDatabase(gdbm.ModeWriter)
	if err != nil {
		log.Fatalf("Can't open database: %v", err)
	}
	defer db.Close()

	args = optset.Args()

	var update []byte

	switch {
	case len(args) == 1:
		if input == "" {
			log.Fatalln("not enough arguments")
		}
		update = updateFromFile(input)
	case len(args) > 1:
		if input != "" {
			log.Fatalln("-i and command line assignments cannot be used together")
		}
		update = updateFromArgs(args[1:])
	default:
		log.Fatalln("not enough arguments")
	}

	err = db.UpdateObject(optset.datatype, args[0], update)
	if err != nil {
		log.Fatalln(err)
	}
}

func Getyn(prompt string, dfl bool) bool {
	rd := bufio.NewReader(os.Stdin)
	yns := `y/N`
	if dfl {
		yns = `Y/n`
	}
	for {
		fmt.Printf("%s [%s]: ", prompt, yns)
		resp, err := rd.ReadString('\n')
		if err != nil {
			log.Fatal(err)
		}
		switch resp[0] {
		case 'y','Y':
			return true
		case 'n','N':
			return false
		case '\n':
			return dfl
		default:
			fmt.Println("Please, answer yes or no")
		}
	}
}

func EditAction(args []string) {
	optset := NewDBOptSet(args)
//	input := ""
//	optset.FlagLong(&input, "input", 'i', "Input file name")
	create := false
	optset.FlagLong(&create, "new", 'n', "Create a new entry")
	optset.SetParameters("KEY")
	optset.Parse()

	db, err := OpenDatabase(gdbm.ModeWriter)
	if err != nil {
		log.Fatalf("Can't open database: %v", err)
	}
	defer db.Close()

	args = optset.Args()
	if len(args) != 1 {
		log.Fatalln("one argument expected")
	}

	key := args[0]

	obj, err := db.FetchObject(optset.datatype, key)
	if err != nil {
		if errors.Is(err, gdbm.ErrItemNotFound) {
			if create {
				obj = db.NewObject(optset.datatype, key)
			} else {
				log.Fatalf("%s: %v; use --new to create it", key, err)
			}
		} else {
			log.Fatalf("%s: %v", key, err)
		}
	}

	orig := object.Clone(obj)

	file, err := ioutil.TempFile("", key + `.*.yml`)
	if err != nil {
		log.Fatalf("can't create temporary file: %v", err)
	}
	filename := file.Name()
	defer os.Remove(filename)

	err = object.Annotate(obj, file)
	if err != nil {
		log.Fatalln(err)
	}
	file.Close()

	editor := os.Getenv("VISUAL")
	if editor == "" {
		editor := os.Getenv("EDITOR")
		if editor == "" {
			editor = "vi"
		}
	}

	for {
		cmd := exec.Command(editor, filename)
		cmd.Stdin = os.Stdin
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr
		if err := cmd.Run(); err != nil {
			log.Fatalf("Error running %s: %v", editor, err)
		}

		content, err := ioutil.ReadFile(filename)
		if err != nil {
			log.Fatalf("%s: %v", filename, err)
		}

		err = yaml.Unmarshal(content, obj)
		if err == nil {
			break
		}
		fmt.Println(err)
		if ! Getyn("Edit again", true) {
			return
		}
	}

	err = db.SyncObject(optset.datatype, obj, orig)
	if err != nil {
		log.Fatalf("Error storing object: %v", err)
	}
}

var vcsRe = regexp.MustCompile(`(svn|git|cvs)`)

func checkRelease(proj *project.Object, force, verbose bool) *project.Object {
	if proj.NewsFormat == "" || proj.NewsFormat == "none" {
		if verbose {
			fmt.Printf("%s: release log disabled", proj.Name())
		}
		return nil
	}
	newsURL := proj.NewsURL
	if newsURL == "" {
		logName := releaselog.LogName(proj.NewsFormat)
		if logName == `` {
			if verbose {
				fmt.Printf("%s: no default log name for format '%s'", proj.Name(), proj.NewsFormat)
			}
			return nil
		}
		url := strings.TrimRight(proj.Repository, `/`)
		vcs := vcsRe.FindString(url)

		switch vcs {
		case `git`:
			newsURL = url + `/plain/` + logName
		case `svn`:
			newsURL = url + `/trunk/` + logName + `/?view=co`
		case `cvs`:
			newsURL = url + `/` + logName + `?view=co`
		default:
			return nil
		}
	}

	if verbose {
		fmt.Printf("Getting log info for %s from %s\n", proj.Name(), newsURL)
	}
	resp, err := http.Get(newsURL)
	if err != nil {
		log.Printf("%s: %v", proj.Name(), err)
		return nil
	}
	defer resp.Body.Close()
	switch resp.StatusCode {
	case 200:
		// Ok
	case 404:
		if verbose {
			fmt.Printf("%s: no release log at %s\n", proj.Name(), newsURL)
		}
		return nil
	default:
		log.Printf("%s: %s: Unexpected response: %s",
			proj.Name(), newsURL, resp.Status)
		return nil
	}
	rlog := releaselog.Parser(resp.Body, proj.NewsFormat, 1)
	if len(rlog) == 0 {
		log.Printf("%s: Empty release log at %s", proj.Name(), newsURL)
		return nil
	}
	if verbose {
		fmt.Printf("Got version %s, released on %s\n", rlog[0].Version, rlog[0].TimeStamp)
	}
	if force || rlog[0].Version != proj.Version {
		proj.Version = rlog[0].Version
		proj.News = strings.Join(rlog[0].Description, "\n")
		proj.ModifiedDate = rlog[0].TimeStamp
		return proj
	}
	return nil
}

func ReleaseLogAction(args []string) {
	verbose := false
	optset := NewGPOptSet(args)
	optset.SetParameters("[PROJECT...]")
	force := false
	optset.FlagLong(&force, "force", 'f', "Force update")
	optset.FlagLong(&verbose, "verbose", 'v', "Verbose mode")
	optset.Parse()

	args = optset.Args()

	db, err := OpenDatabase(gdbm.ModeWriter)
	if err != nil {
		log.Fatalf("Can't open database: %v", err)
	}
	defer db.Close()

	signal_chan := make(chan os.Signal, 1)
	signal.Notify(signal_chan, syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)

	go func() {
		select {
		case <-signal_chan:
			db.Close()
			os.Exit(0)
		}
	}()

	upd := []*project.Object{}

	if len(args) > 0 {
		for _, key := range args {
			proj, err := db.FetchProject(key)
			if err != nil {
				log.Fatalf("Can't fetch project %s: %v", key, err)
			}
			newproj := checkRelease(proj, force, verbose)
			if newproj != nil {
				upd = append(upd, newproj)
			}
		}
	} else {
		next := db.Iterator(ProjectsType)
		var key string
		for key, err = next(); err == nil; key, err = next() {
			proj, err := db.FetchProject(key)
			if err != nil {
				log.Fatalf("Can't fetch project %s: %v", key, err)
			}
			newproj := checkRelease(proj, force, verbose)
			if newproj != nil {
				upd = append(upd, newproj)
			}
		}
		if !errors.Is(err, gdbm.ErrItemNotFound) {
			log.Fatalf("Iteration finished abnormally: %v", err)
		}
	}

	if verbose {
		fmt.Printf("Updating %d projects\n", len(upd))
	}
	for _, proj := range upd {
		err = db.StoreProject(proj, true)
		if err != nil {
			log.Fatalf("Failed to update %s: %v", proj.Name(), err)
		}
	}
}

func ReleaseLogParserAction(args []string) {
	optset := NewGPOptSet(args)
	optset.SetParameters("FILE")
	format := "any"
	optset.FlagLong(&format, "format", 'H', "Release log format")
	limit := 0
	optset.FlagLong(&limit, "limit", 'n', "Limit the number of file entries to scan")
	terse := false
	optset.FlagLong(&terse, "terse", 't', "Terse mode (don't show log entry text)")
	optset.Parse()

	args = optset.Args()
	if len(args) != 1 {
		log.Fatalln("bad arguments")
	}

	file, err := os.Open(args[0])
	if err != nil {
		log.Fatalf("%s: %v", args[0], err)
	}

	rlog := releaselog.Parser(file, format, limit)

	fmt.Printf("%d log entries\n", len(rlog))
	for i, hent := range rlog {
		fmt.Printf("%d: Version %s, Release date: %s, News lines: %d\n", i, hent.Version, hent.TimeStamp, len(hent.Description))
		if ! terse {
			fmt.Println(strings.Join(hent.Description, "\n"))
		}
	}
}

func IndexAction(args []string) {
	optset := NewGPOptSet(args)
	optset.SetParameters("")
	optset.Parse()

	args = optset.Args()
	if len(args) != 0 {
		log.Fatalln("bad arguments")
	}

	db, err := OpenDatabase(gdbm.ModeWriter)
	if err != nil {
		log.Fatalf("Can't open database for writing: %v", err)
	}
	defer db.Close()

	err = db.Reindex()
	if err != nil {
		log.Fatal(err)
	}
}

type Action struct {
	Action func ([]string)
	Help string
}

var actions map[string]Action

func HelpAction([]string) {
	commands := make([]string, len(actions))
	i := 0
	for com := range actions {
		commands[i] = com
		i++
	}
	sort.Strings(commands)
	fmt.Printf("usage: %s COMMAND [ARGS...]\n", filepath.Base(os.Args[0]))
	fmt.Printf("Available commands:\n")
	for _, com := range commands {
		fmt.Printf("    %-10s  %s\n", com, actions[com].Help)
	}
	fmt.Printf("To obtain a help on a particular command, run: %s COMMAND -h\n", filepath.Base(os.Args[0]))
}

func main() {
	ReadConfig()
	optset := getopt.New()
	optset.SetProgram(filepath.Base(os.Args[0]))
	optset.SetParameters("COMMAND [ARGS...]")
	optset.FlagLong(&Config.RootDir, "directory", 'C', "Root directory")
//	optset.FlagLong(&verbose, "verbose", 'v', "Verbose mode")
	if err := optset.Getopt(os.Args, nil); err != nil {
		fmt.Fprintln(os.Stderr, err)
		optset.PrintUsage(os.Stderr)
		os.Exit(1)
	}

	args := optset.Args()

	actions = map[string]Action{
		"list":    Action{Action: ListAction,
				  Help: "List database content"},
		"load":    Action{Action: LoadAction,
				  Help: "Load database entries from one or more JSON files"},
		"dump":    Action{Action: DumpAction,
				  Help: "Dump database content as a JSON object"},
		"delete":  Action{Action: DeleteAction,
				  Help: "Delete an entry from the database"},
		"update":  Action{Action: UpdateAction,
				  Help: "Modify a database entry"},
		"version": Action{Action: VersionAction,
				  Help: "Print program and GDBM version number"},
		"rlog":    Action{Action: ReleaseLogAction,
				  Help: "Update from release logs"},
		"parselog":Action{Action: ReleaseLogParserAction,
				  Help: "Parse a release log file and exit"},
		"server":  Action{Action: ServerAction,
				  Help: "Run http server"},
		"edit":    Action{Action: EditAction,
				  Help: "Edit existing database entry or create a new one"},
		"index":   Action{Action: IndexAction,
				  Help: "Index related projects and categories"},
		"help":    Action{Action: HelpAction,
			          Help: "Show a short help summary"},
	}

	log.SetPrefix(filepath.Base(os.Args[0]) + ": ")
	log.SetFlags(log.Lmsgprefix)
	if len(os.Args) == 1 {
		log.Fatalf("command missing; try `%s help' for assistance", filepath.Base(os.Args[0]))
	}

	if act, ok := actions[args[0]]; ok {
		act.Action(args)
		os.Exit(0)
	}

	log.Fatalf("unrecognized action; try `%s help' for assistance", filepath.Base(os.Args[0]))
}
