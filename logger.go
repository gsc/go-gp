package main

import (
	"os"
	"io"
	"fmt"
	"net"
	"time"
	"bufio"
	"bytes"
	"strings"
	"net/http"
	"sync"
	"log"
	"github.com/felixge/httpsnoop"
	"github.com/lestrrat-go/strftime"
)

const (
	TextSegment = iota
	ConvSegment
)

type Segment interface{}

type SegmentText struct {
	Text string
}

type SegmentConv struct {
	Format func(w io.Writer, m httpsnoop.Metrics, r *http.Request)
}

type HTTPLogger struct {
	output *os.File
	mutex sync.Mutex
	timeFmt map[string]*strftime.Strftime
	endTime time.Time
	segments []Segment
}

func NewHTTPLogger() *HTTPLogger {
	return &HTTPLogger{output: os.Stdout, mutex: sync.Mutex{},
		timeFmt: make(map[string]*strftime.Strftime)}
}

func (hl *HTTPLogger) addText(text string) {
	segm := SegmentText{Text: text}
	hl.segments = append(hl.segments, segm)
}

func requestGetRemoteIP(r *http.Request) string {
	xff := r.Header.Get("X-Forwarded-For")
	if xff != "" {
		for _, ip := range strings.Split(xff, ",") {
			ip = strings.TrimSpace(ip)
			if ! Config.IsTrustedIP(ip) {
				return ip
			}
		}
	}
	ip, _, err := net.SplitHostPort(r.RemoteAddr)
	if err != nil {
		ip = r.RemoteAddr
	}
	return ip
}

func (hl *HTTPLogger) addFormat(c byte, q string) {
	segm := SegmentConv{}
	switch c {
	case 'a':
		if q == "c" {
			segm.Format = func(w io.Writer, m httpsnoop.Metrics, r *http.Request) {
				ip, _, err := net.SplitHostPort(r.RemoteAddr)
				if err != nil {
					ip = r.RemoteAddr
				}
				fmt.Fprint(w, ip)
			}
		} else {
			segm.Format = func(w io.Writer, m httpsnoop.Metrics, r *http.Request) {
				fmt.Fprint(w, requestGetRemoteIP(r))
			}
		}

	case 'A':
		segm.Format = func(w io.Writer, m httpsnoop.Metrics, r *http.Request) {
			ip, _, err := net.SplitHostPort(Config.Address)
			if err != nil {
				ip = "0.0.0.0"
			}
			fmt.Fprint(w, ip)
		}

	case 'B':
		segm.Format = func(w io.Writer, m httpsnoop.Metrics, r *http.Request) {
			fmt.Fprint(w, m.Written)
		}

	case 'b':
		segm.Format = func(w io.Writer, m httpsnoop.Metrics, r *http.Request) {
			if m.Written == 0 {
				fmt.Fprint(w, "-")
			} else {
				fmt.Fprint(w, m.Written)
			}
		}

	case 'D':
		segm.Format = func(w io.Writer, m httpsnoop.Metrics, r *http.Request) {
			fmt.Fprint(w, m.Duration.Microseconds())
		}

	case 'e':
		segm.Format = func(w io.Writer, m httpsnoop.Metrics, r *http.Request) {
			fmt.Fprint(w, os.Getenv(q))
		}

	case 'h':
		segm.Format = func(w io.Writer, m httpsnoop.Metrics, r *http.Request) {
			fmt.Fprint(w, r.RemoteAddr)
		}

	case 'H':
		segm.Format = func(w io.Writer, m httpsnoop.Metrics, r *http.Request) {
			fmt.Fprint(w, r.Proto)
		}

	case 'i':
		segm.Format = func(w io.Writer, m httpsnoop.Metrics, r *http.Request) {
			fmt.Fprint(w, r.Header.Get(q))
		}

	case 'l','u':
		segm.Format = func(w io.Writer, m httpsnoop.Metrics, r *http.Request) {
			fmt.Fprint(w, "-")
		}

	case 'm':
		segm.Format = func(w io.Writer, m httpsnoop.Metrics, r *http.Request) {
			fmt.Fprint(w, r.Method)
		}

	case 'p':
		segm.Format = func(w io.Writer, m httpsnoop.Metrics, r *http.Request) {
			_, port, err := net.SplitHostPort(Config.Address)
			if err != nil {
				port = "80"
			}
			fmt.Fprint(w, port)
		}

	case 'q':
		segm.Format = func(w io.Writer, m httpsnoop.Metrics, r *http.Request) {
			if r.URL.RawQuery != "" {
				fmt.Fprintf(w, "?%s", r.URL.RawQuery)
			}
		}

	case 'r':
		segm.Format = func(w io.Writer, m httpsnoop.Metrics, r *http.Request) {
			fmt.Fprint(w, r.URL.String(), " ", r.Proto)
		}

	case 'R':
		// FIXME: handler name

	case 's':
		segm.Format = func(w io.Writer, m httpsnoop.Metrics, r *http.Request) {
			fmt.Fprint(w, m.Code)
		}

	case 't':
		segm.Format = func(w io.Writer, m httpsnoop.Metrics, r *http.Request) {
			t := hl.endTime
			qs := q
			if strings.HasPrefix(qs, "begin:") {
				qs  = strings.TrimPrefix(qs, "begin:")
				t = t.Add(- m.Duration)
			} else if strings.HasPrefix(qs, "end:") {
				qs  = strings.TrimPrefix(qs, "end:")
			} else {
				t = t.Add(- m.Duration)
			}
				
			switch qs {
			case "":
				fmt.Fprint(w, t.Format("[02/Jan/2006 15:04:05 -0700]"))
			case "sec":
				fmt.Fprint(w, t.Unix())

			case "msec":
				//FIXME: for 1.18 and later:
				//fmt.Fprint(w, t.UnixMicro())
				fmt.Fprint(w, t.UnixNano() / 1e3)

			case "usec":
				//fmt.Fprint(w, t.UnixMilli())
				fmt.Fprint(w, t.UnixNano() / 1e6)

			case "msec_frac":
				fmt.Fprintf(w, "%f", float64(t.UnixNano())/1e3)

			case "usec_frac":
				fmt.Fprintf(w, "%f", float64(t.UnixNano())/1e6)

			default:
				sft, ok := hl.timeFmt[q]
				if ! ok {
					var err error
					sft, err = strftime.New(q)
					if err != nil {
						log.Printf("%s: %v", q, err)
						sft, err = strftime.New("%c")
						if err != nil {
							log.Fatal("can't use %%{%s}T nor %%{%%c}T in logs; last error: %v")
						}
					}
					hl.timeFmt[q] = sft
				}
				sft.Format(w, t)
			}
		}

	case 'T':
		switch q {
		case `ms`:
			segm.Format = func(w io.Writer, m httpsnoop.Metrics, r *http.Request) {
				fmt.Fprint(w, m.Duration.Microseconds())
			}
		case `us`:
			segm.Format = func(w io.Writer, m httpsnoop.Metrics, r *http.Request) {
				fmt.Fprint(w, m.Duration.Milliseconds())
			}
		case `s`,``:
			segm.Format = func(w io.Writer, m httpsnoop.Metrics, r *http.Request) {
				fmt.Fprint(w, m.Duration.Seconds())
			}
		}

	case 'U':
		segm.Format = func(w io.Writer, m httpsnoop.Metrics, r *http.Request) {
			fmt.Fprint(w, r.URL.Path)
		}

	default:
		segm.Format = func(w io.Writer, m httpsnoop.Metrics, r *http.Request) {
			fmt.Fprint(w, "%")
			if q != "" {
				fmt.Fprintf(w, "{%s}", q)
			}
			fmt.Fprintf(w, "%c", c)
		}
	}
	hl.segments = append(hl.segments, segm)
}

func (hl *HTTPLogger) Parse(format string) {
	split := func(data []byte, atEOF bool) (advance int, token []byte, err error) {
		n := bytes.Index(data, []byte(`%`))
		switch n {
		case 0:
			if data[1] == '{' {
				k := bytes.Index(data, []byte(`}`))
				if k == -1 {
					n = 2
				} else if k + 2 < len(data) {
					n = k + 2
				} else {
					n = k + 1
				}
			} else if data[1] == '>' && data[2] == 's' {
				token = []byte{'%', 's',}
				advance = 3
				return
			} else {
				n = 2
			}

		case -1:
			n = len(data)
		}

		token = data[:n]
		advance = n
		if n == len(data) {
			err = bufio.ErrFinalToken
		}
		return
	}


	scanner := bufio.NewScanner(strings.NewReader(format))
	scanner.Split(split)
	hl.segments = []Segment{}
	for scanner.Scan() {
		tok := scanner.Bytes()
		if tok[0] == '%' {
			switch {
			case tok[1] == '%':
				hl.addText(`%`)
			case tok[1] == '{' && len(tok) == 2:
				hl.addText(string(tok))
			case tok[1] == '{':
				hl.addFormat(tok[len(tok)-1], string(tok[2:len(tok)-2]))
			default:
				hl.addFormat(tok[1], "")
			}
		} else {
			hl.addText(string(tok))
		}
	}
}

func (hl *HTTPLogger) Format(m httpsnoop.Metrics, r *http.Request) {
	hl.mutex.Lock()
	defer hl.mutex.Unlock()
	hl.endTime = time.Now()
	if hl.output == nil {
		return
	}
	for _, s := range hl.segments {
		if st, ok := s.(SegmentText); ok {
			fmt.Fprint(hl.output, st.Text)
		} else if sc, ok:= s.(SegmentConv); ok {
			sc.Format(hl.output, m, r)
		}
	}
	fmt.Fprintln(hl.output)
}

func (hl *HTTPLogger) Close() {
	hl.mutex.Lock()
	defer hl.mutex.Unlock()
	if hl.output != nil && hl.output != os.Stdout {
		hl.output.Close()
	}
}

func (hl *HTTPLogger) Open(filename string) (err error) {
	hl.mutex.Lock()
	defer hl.mutex.Unlock()

	if hl.output != nil && hl.output != os.Stdout {
		hl.output.Close()
	}

	if filename == "" || filename == "-" {
		hl.output = os.Stdout
	} else {
		hl.output, err = os.OpenFile(filename, os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0666)
	}
	return
}

func (hl *HTTPLogger) OpenDefault() error {
	hl.Parse(Config.LogFormat)
	return hl.Open(Config.AccessLog)
}
