# gp

This is a driver for Gray's Pages - personal web space of Sergey
Poznyakoff.  The web space is organized as a tool that assists the
author in managing the plethora of projects he is maintaining and
developing.  It contains some static pages too, but these are of
secondary importance.

The driver comes as a single binary `gp`, that is invoked as follows:

```
   gp COMMAND [ARGS...]
```

where _COMMAND_ is a command to perform.

## Configuration file.

When started, `gp` looks for its configuration file in the current
working directory.  The file name is `gp.conf`.  It is a YAML file,
with the following structure:

* `RootDir:` _string_

   Specifies the root directory for the pages.  This is a directory
   where the rest of files and directories is being looked up, unless
   their names begin with a slash.

   Defaults to current working directory.

* `DBDir:` _string_

   Database directory.  In this directory two GDBM database files are
   located: `projects.db` and `categories.db`.

   Defaults to `$RootDir/db`.

<a name="TemplateDir"></a>
* `TemplateDir:` _string_

   Web page template directory.  The default value is `$RootDir/tmpl`.
   This directory should contain the following template files:

   * `default.tmpl`

     Default web page layout.

   * `catlist.tmpl`

     Template for the category list page.

   * `catprojects.tmpl`

     Template for a page listing all projects in a category.
     
   * `dir.tmpl`

     Template for the project directory page.

   * `project.tmpl`

     Template for the single project page.

   * `projectlist.tmpl`

     Template for the project list page.

* `StaticDir:` _string_

   Directory for static files.  Defaults to `$RootDir/static`.
   It must contain at least the css file `css/style.css`.  In default
   setup, it contains also two files included by it:
   `css/stylestd.css` and `css/stylemob.css`.

   Other static files, such as `index.html` etc., should also be
   located there.

* `ErrorDir:` _string_

   Directory for customized HTTP error pages: static HTML files named
   `CODE.html`, where _CODE_ is the HTTP error code.  As of gp version
   1.2, it can contain the following files:

   * 404.html

   * 500.html

   * 503.html

   Default: `$RootDir/error`.

* `Address`: _address_

   Address to listen on for the `gp server` command.  The default is
   `:8080`.  See [net.Dial](https://pkg.go.dev/net#Dial) for the
   address format.

* `ProjectDirURI:` _string_

   URI of the project directory page.  Defaults to `/dir`.

* `ProjectListURI:` _string_

   URI of the project list page.  Defaults to `/project`.

* `CategoryListURI:` _string_

   URI of the category list page.  Defaults to `/category`.

<a name="TrustedIP"></a>
* `TrustedIP:` _collection of CIDRs_

   A collection of IP addresses or CIDRs of the trusted proxies.  When
   handling requests from these IP addresses, the `X-Forwarded-For`
   header will be honored, so that the `%a` conversion in the
   `LogFormat` (see the [Log Format](#user-content-log-format)) will
   expand to the actual IP address of the requester.

   Default value: `127.0.0.1`

* `AccessLog:` _string_

   File name of the HTTP access log file.  Empty value or minus sign
   (`-`) stands for the standard output (the default).

* `LogFormat:` _string_

   Format of the access log entries.  Refer to [Log Format](#user-content-log-format)
   for a detailed description.  The default is: `%a %l %u %t "%r" %>s %b "%{Referer}i" "%{User-Agent}i"`.

* `RewriteRules:` _collection_

   A collection of _rewrite rules_, which are applied to each incoming
   request before routing it.  See [Rewrite Rules](#user-content-rewrite-rules) for
   a detailed discussion.

   Default is empty (no rules).

* `RewriteDebug:` _bool_

   When set to `true`, a detailed transcript of applying the rewrite
   rules will be printed on standard error for each incoming request.

   Default is `false`.

## Database files

The pages operate on two database files in GDBM format: _project_ and _category_. They are
located in the `DBDir` directory and named `projects.db` and `categories.db`. Both files
are indexed by the project or category name, without terminating '\0' character. Values are
JSON strings that map to the corresponding structures.

See the files `project/project.go` and `category/category.go` for the data type and JSON
mapping definitions.

## Commands

The generalized command syntax is

```
  gp command [OPTIONS] [ARGS...]
```

Each _command_ (except `version`) understands the `-h` option or its long equivalent `--help`.
This option causes the command to display a short usage summary and exit with 0 status.

Most _commands_ operate on a single relation (database file) or entity.  By default, it is
project entities.  Two options are common to such commands and provide a way of selecting
a relation or entity to operate upon:

* `-c`, `--category`

  Operate on categories.

* `-p`, `--project`

  Operate on projects (default).  This option is provided for completeness.

In the subsections below we discuss each command in detail.

### gp delete

```
 gp delete [OPTIONS] KEY
```

Options:

* `-c`, `--category`

  Use the categories database.

* `-p`, `--project`

  Use the projects database (default).

Deletes an entry from the database.

### gp dump

```
 gp dump [OPTIONS]
```

Options:

* `-o`, `--output=`_NAME_

  Sets the output file name.

Dumps the database content as a JSON object.  By default, the result is printed on
standard output.  To write it to a file, use the `--output` (`-o`) option.

This command can be used to create backup copies of the databases, e.g.:

```
  gp dump -o gp.json
```

The dumps created with this command can later be used to recreate the database using
the `gp load` command (see below).

### gp edit

```
 gp edit [OPTIONS] KEY
```

Options:

* `-c`, `--category`

  Use the categories database.

* `-p`, `--project`

  Use the projects database (default).

* `-n`, `--new`

  Create a new entry.

The `gp edit` command retrieves the entry for the key _KEY_ from the database, formats it as
annotated YAML and loads it into the editor specified by the `VISUAL` or `EDITOR` environment
variables.  After the user edits the entry, saves the changes and exits from the editor, the
edited content is converted back to the internal representation and stored in the database,
replacing old content for _KEY_.

To create a new entry, use the `--new` (`-n`) option.

### gp help

Displays a short help summary, that lists the available commands along with a one-line
description for each of them.

### gp index

```
  gp index
```

This command synchronizes `Related` and `Category` attributes entries in the project database.
Most of the time it is not needed, as these attributes should be synchronized automatically on
each update.  Use it if the database goes out of sync for some reason.

### gp list

```
 gp list [OPTIONS]
```

Options:

* `-c`, `--category`

  Use the categories database.

* `-p`, `--project`

  Use the projects database (default).

* `-F`, `--format=`_FORMAT_

  Output format.

Lists the content of the database on standard output.  The _FORMAT_, if supplied, can be
either a valid golang format string, or a name of the file prefixed with a `@` sign.  In
the latter case, the file's content is loaded and used as format.

The supplied format is applied to each database entry in turn.  The format can make
use of the following additional functions:

* `json` (one argument)

  Formats the argument as a JSON.

* `yaml` (one argument)

  Formats the argument as a YAML.

* `name` (entry)

  Takes a single database entry as its argument.  Returns the name (database key) for that
  entry.

* `nl`

  Outputs a newline.

The default format is

```
  {{ name . }} {{ json . }}
```

### gp load

```
  gp load [OPTIONS] [FILE...]
```

* `-P`, `--purge`

  Remove all existing records from the database before loading new ones.

Loads database entries from one or more JSON files supplied in the command line.  If no _FILE_ is
given, reads standard input.

The input files should have been created by [gp dump](#user-content-gp-dump).  The commend
is intended to restore a database from the backup copy created by `gp dump`.  It will specifically
refuse to overwrite existing entries in the database (unless given the `--purge` option, in which
case it will destroy the existing database prior to loading into it).

### gp rlog

```
  gp rlog [PROJECT...]
```

Options:

* `-f`, `--file=`_NAME_

  Database file name.

* `-v`, `--verbose`

  Verbose mode

This command updates the project database, by scanning the repositories of each project and
looking for the information about its recent release.  If that release is newer than the one
recorded in the database, the following project attributes are updated with the new ones:
`modified_date`, `news`, and `version`.  If one or more project names are supplied in the
command line, only these projects will be updated.

This command is intended to be run periodically, as a cronjob.

### gp server

```
  gp server [ADDRESS]
```

Runs gp HTTP server.  By default, the server listens on port 8080 on all available interfaces.
To listen on a specific IP address and port, give it as an argument (using the [net.Dial](https://pkg.go.dev/net#Dial) format), e.g.:

```
  gp server 127.0.0.1:8090
```

### gp update

```
  gp update [OPTIONS] KEY [ATTR:VALUE...]
```

Options:

* `-c`, `--category`

  Use the categories database.

* `-p`, `--project`

  Use the projects database (default).

* `-i`, `--input=`_NAME_

  Read changeset from the file _NAME_.

Modifies existing database entry.  The attributes to change (_changeset_) are either supplied
on the command line, or in a file given by the `--input` (`-i`) options.

When supplied in the command line, each argument is a _ATTR_:_VALUE_ pair.  It must be formatted
as if it were a part of a JSON object declaration, except that there's no need to quote _ATTR_ and
_VALUE_, unless required by the shell syntax.  For example:

```
  gp update foobar version:8.13 category:'["gray","mail"]'
```

When using input file, it must be formatted as a JSON object.  Only attributes present in that
object will be changed.  The above example can be formatted as:

```
{ "version":"8.13", "category":["gray","mail"] }
```

### gp version

Prints program and GDBM version number.

## Templates

Templates for dynamic pages are located in [TemplateDir](#user-content-TemplateDir). These are
HTML pages written using the golang [HTML templates](https://pkg.go.dev/html/template).  The
following `gp`-specific functions are available for use in all templates:

* `odd` _number_ : _bool_

  Returns `true` if _number_ is odd.

* `even` _number_ : _bool_

  Returns `true if _number_ is even.

* `decr` _number_ : _int_

  Returns _number_ decremented by one.

* `incr` _number_ : _int_

  Returns _number_ incremented by one.

* `ThisURL` : [url.URL](https://pkg.go.dev/net/url#URL)

  Returns the request URL.

* `query` _name_: _string_

  Returns the value of the query parameter _name_.

* `clearquery` _names_ ... : [url.URL](https://pkg.go.dev/net/url#URL)

  Returns a URL obtained from the request URL (see `ThisURL`) by removing query
  parameters named in its arguments (_names_).

* `sort` _[]string_ : _[]string_

  Returns a sorted array.

  __Notice:__ modifies its argument.

* `Config` : _*GPConfig_

  Returns a reference to the current configuration.  See `config.go`.

* `markdown` _text_ : [template.HTML](https://pkg.go.dev/html/template#HTML)

  Formats _text_ as a markdown and returns the resulting string.  The return will be
  included verbatim in the template output.

* now : [time.Time](https://pkg.go.dev/time#Time)

  Returns current time.

When building dynamic page, `gp` first processes the _layout template_ `default.tmpl`.  When
processing it, the template for the actual page is registered under the name "content".  The
layout template normally contains the following:

```
  {{ template "content" .Arg }}
```

The `.Arg` parameter is defined to be a template-specific argument.

The "content" template, in addition to the functions described above, can use the following:

* `json` _obj_ : _string_

  Formats _obj_ as a JSON value.

* `name` _obj_ : _string_

  Argument must be a GP object (see `object/object.go`).  The function returns its name.

* `get` _key_ : _obj_

  Looks up the _key_ in the current database and returns the corresponding object.  _Current_
  database is the database file associated with the content template.

* `names` _relation_ _sort_ : _[]string_

  Returns all keys from the _relation_ sorted according to the _sort_ string.  The _relation_
  argument must be either _"project"_ or _"category"_.  The  sort string consists of two characters:
  sort attribute and sort ordering.  The former is `n` to sort by name and `d` to sort by release
  date (only for projects).  The latter is `a` for ascending and `d` for descending sort.  Missing
  sort ordering defaults to `a` and missing sort attribute defaults to `n`.

* `wildmatch` _pattern_ _[]string_ : _[]string_

  Returns an array containing all elements from its second argument that match the globbing
  pattern in its first argument.

## Log Format

* `%%`

	The percent sign.

* `%a`

	Client IP address of the request.  It is determined by
	consulting the `X-Forwarded-For` headers sent from trusted
	proxies.  The list of trusted proxies is determined by the [TrustedIP](#user-content-TrustedIP)
	configuration file statement.

* `%{c}a`

	Underlying peer IP address of the connection.

* `%A`

	Local IP-address.

* `%B`

	Size of response in bytes, excluding HTTP headers.

* `%b`

	Size of response in bytes, excluding HTTP headers. In CLF format,
	i.e. a '-' rather than a 0 when no bytes are sent.

* `%D`

	The time taken to serve the request, in microseconds.

* `%{VARNAME}e`

	The contents of the environment variable _VARNAME_.

* `%h`

	The IP address of the remote client.  Notice, that if `gp` is running
	behind a proxy, this will be the IP of the proxy, not the actual client.
	Use `%a` instead.

* `%H`

	The request protocol.

* `%{VARNAME}i`

	The contents of _VARNAME:_ header line(s) in the request sent to
	the server.

* `%l`

	This is intended to be the remote logname (from `identd`, if supplied).
	As of version 1.0, this conversion always returns a dash.

* `%m`

	The request method.

* `%p`

	The canonical port of the server serving the request.

* `%q`

	The query string (prepended with a `?` if a query string exists,
	otherwise an empty string).

* `%r`

	First line of request.

* `%s` or `%>s`

	HTTP response status.

* `%t`

	Time the request was received, in the format
	`[18/Sep/2011:19:18:28 -0400]`. The last number indicates the
	timezone offset from GMT

* `%{format}t`

	The time, in the form given by format, which should be in an
	extended `strftime`(3) format. If the format starts with `begin:`
	(default) the time is taken at the beginning of the request processing.
	If it starts with `end:` it is the time when the log entry gets written,
	close to the end of the request processing. In addition to the formats
	supported by `strftime`(3), the following format tokens are supported:

	* `sec`

		number of seconds since the Epoch

	* `msec`

		number of milliseconds since the Epoch

	* `usec`

		number of microseconds since the Epoch

	* `msec_frac`

		millisecond fraction

	* `usec_frac`

		microsecond fraction

	These tokens can not be combined with each other or
	`strftime`(3) formatting in the same format string. You can use
	multiple `%{format}t` tokens instead.

* `%T`

	The time taken to serve the request, in seconds.

* `%{UNIT}T`

	The time taken to serve the request, in a time unit given by
	_UNIT_. Valid units are `ms` for milliseconds, `us` for
	microseconds, and `s` for seconds. Using `s` gives the same result
	as `%T` without any format; using `us` gives the same result as
	`%D`.

* `%U`
	The URL path requested, not including any query string.


## Rewrite Rules

Rewrite rules allow the `gp server` command to alter the path part of the requested
URI to refer to another local or remote resource.  The rules are stored as a collection
of statements in the `RewriteRules` configuration statement.  Syntactically, a rewrite
rule consists of two to three parts separated by whitespace:

```
   PATTERN REPLACEMENT [FLAGS]
```

The first part, _PATTERN_, is a golang regexp that the incoming URI path must match in order
to trigger the rule.  If the rule is triggered, the path is replaced with the value of _REPLACEMENT_,
after expanding the parenthesized group references in form `$N`, where _N_ is a 1-based group
index.  By default, after a rule had matched and path was rewritten, control passes to the rule
that follows.  That can be changed using optional _FLAGS_.  As of version 1.2, following flags
are defined:

* `last`

  Stop processing if this rule matches.  Serve the modified path.

* `301` or `permanent`

  Return a 301 response (permanent redirect).  Implies `last`.

* `302` or `temporary`

  Return a 302 response (temporary redirect).  Implies `last`.

Consider, for example the following rules:

```
   RewriteRules:
     - ^/$ /index.html last
     - ^/list(/.*)? /project$1
     - ^/project/foo /project/bar last
     - ^/gnu$ https://www.gnu.org permanent

```

The first rule will match requests with empty paths (i.e. with a `/` as a path).  Thus, for
example `http://gp.example.com/` will be rewritten as `http://gp.example.com/index.html`.  This
rule returns immediately if matched.

Second rule changes the initial `/list` prefix in the path to `/project`.  If that rule matches,
control is passed to rule 3 with the rewritten path.

The rule 3 rewrites `/project/foo` with `/project/bar` and returns immediately.

Finaly, the last rule returns a 301 redirect to `https://www.gnu.org` if the incoming path is
`/gnu`.
