package main

import (
	"fmt"
	"io"
	"log"
	"errors"
	"github.com/graygnuorg/go-gdbm"
	"gp/object"
	"gp/category"
	"gp/project"
	"text/template"
	"encoding/json"
	"path/filepath"
	"sort"
)

const (
	ProjectsType = iota
	CategoryType
	NumTypes
)

type RelDef struct {
	id string
	filename string
	obj object.Object
}

var RelDefs = []RelDef{
	{ "project",  "projects.db",   &project.Object{} },
	{ "category", "categories.db", &category.Object{} },
}

func FindRelation(id string) (int, bool) {
	for i, _ := range RelDefs {
		if RelDefs[i].id == id {
			return i, true
		}
	}
	return 0, false
}

type Database struct {
	relation []*object.Database
	lastErr error
}

func OpenRelation(reltype int, mode int) (*object.Database, error) {
	return object.Open(filepath.Join(Config.RootDir, Config.DBDir, RelDefs[reltype].filename), mode, RelDefs[reltype].obj)
}

func OpenDatabase(mode int) (db *Database, err error) {
	db = &Database{relation: make([]*object.Database, NumTypes)}
	for i, _ := range RelDefs {
		db.relation[i], err = OpenRelation(i, mode)
		if err != nil {
			err = fmt.Errorf("can't open relation %d: %v", i, err)
			return
		}
	}
	return
}

func (db *Database) Close() {
	for i, _ := range RelDefs {
		if db.relation[i] != nil {
			db.relation[i].Close()
		}
	}
}

func (db *Database) LastError() error {
	return db.lastErr
}

func (db *Database) FetchObject(rel int, key string) (obj object.Object, err error) {
	obj, err = db.relation[rel].FetchObject(key)
	if err != nil {
		db.lastErr = err
	}
	return
}

func (db *Database) StoreObject(rel int, obj object.Object, replace bool) (err error) {
	err = db.relation[rel].StoreObject(obj, replace)
	if err != nil {
		db.lastErr = err
	}
	return
}

func (db *Database) UpdateObject(rel int, name string, update []byte) error {
	obj, orig, err := db.relation[rel].UpdateObject(name, update)
	if err != nil {
		db.lastErr = err
		return err
	}
	return db.IndexObject(rel, obj, orig)
}

func (db *Database) DeleteObject(rel int, name string) (err error) {
	err = db.relation[rel].DeleteObject(name)
	if err != nil {
		db.lastErr = err
	}
	return
}

func (db *Database) NewObject(rel int, key string) object.Object {
	return db.relation[rel].NewObject(key)
}

func (db *Database) Iterator(rel int) object.DatabaseIterator {
	return db.relation[rel].StringIterator()
}

func (db *Database) List(rel int, tmpl *template.Template) error {
	return db.relation[rel].List(tmpl)
}

func (db *Database) Dump(wr io.Writer) error {
	io.WriteString(wr, "{")
	delim := "\n"
	for i, _ := range RelDefs {
		io.WriteString(wr, delim + `"` + RelDefs[i].id + `":`)
		if err := db.relation[i].Dump(wr); err != nil {
			return err
		}
		delim = ",\n"
	}
	io.WriteString(wr, "}\n");
	return nil
}

func (db *Database) Load(rd io.Reader) (err error) {
	var km map[string]json.RawMessage

	dec := json.NewDecoder(rd)
	err = dec.Decode(&km)
	if err != nil {
		return
	}
	for rel, msg := range km {
		t, found := FindRelation(rel)
		if !found {
			return fmt.Errorf("Unknown relation: %s", rel)
		}
		var objs map[string]json.RawMessage
		err = json.Unmarshal(msg, &objs)
		if err != nil {
			return
		}
		for key, msg := range objs {
			var obj object.Object
			obj, err = db.relation[t].Unmarshal(key, msg)
			if err != nil {
				return
			}
			err = db.relation[t].StoreObject(obj, false)
			if err != nil {
				return
			}
		}
	}

	// for i, _ := range RelDefs {
	//	next := db.Iterator(i)
	//	var key string
	//	for key, err = next(); err == nil; key, err = next() {
	//		obj, err := db.FetchProject(key)
	//		if err != nil {
	//			log.Fatalf("Can't fetch object %d:%s: %v", i, key, err)
	//		}
	//		db.IndexObject(i, obj)
	//	}
	// }

	return nil
}

func (db *Database) SyncObject(rel int, obj, orig object.Object) (err error) {
	err = db.StoreObject(rel, obj, true)
	if err != nil {
		db.lastErr = err
		return err
	}
	return db.IndexObject(rel, obj, orig)
}

func (db *Database) IndexObject(rel int, obj, orig object.Object) error {
	switch rel {
	case ProjectsType:
		return db.IndexProject(obj.(*project.Object), orig.(*project.Object))
	case CategoryType:
		return db.IndexCategory(obj.(*category.Object), orig.(*category.Object))
	}
	return nil
}

func (db *Database) SortedKeys(rel int, sortby string, sortord string) (result []string, err error) {
	return db.relation[rel].SortedKeys(sortby, sortord)
}

func (db *Database) FetchProject(key string) (*project.Object, error) {
	obj, err := db.FetchObject(ProjectsType, key)
	if err != nil {
		db.lastErr = err
		return nil, err
	}
	return obj.(*project.Object), nil
}

func HasString(a []string, key string) bool {
	for i := range a {
		if a[i] == key {
			return true
		}
	}
	return false
}

// Compute differences between two sorted string arrays.
func ArrayDiff(a, b []string) (rem []string, add []string, same []string) {
	i := 0
	j := 0
	for {
		switch {
		case i == len(a):
			add = append(add, b[j:]...)
			return

		case j == len(b):
			rem = append(rem, a[i:]...)
			return

		case a[i] == b[j]:
			same = append(same, a[i])
			i += 1
			j += 1

		case a[i] < b[j]:
			rem = append(rem, a[i])
			i += 1

		case a[i] > b[j]:
			add = append(add, b[j])
			j += 1
		}
	}
	return
}

//FIXME: lastErr?
func (db *Database) IndexProject(proj, orig *project.Object) error {
	// Update categories.
	rem, add, _ := ArrayDiff(orig.Category, proj.Category)
	for _, name := range rem {
		cat, err := db.FetchCategory(name)
		if err == nil {
			i := sort.SearchStrings(cat.Projects, proj.Name())
			if i < len(cat.Projects) && cat.Projects[i] == proj.Name() {
				cat.Projects = append(cat.Projects[:i], cat.Projects[i+1:]...)
				err = db.StoreCategory(cat, true)
				if err != nil {
					return fmt.Errorf("error storing category %s: %v", name, err)
				}
			}
		} else if errors.Is(err, gdbm.ErrItemNotFound) {
			//log.Printf("warning: project %s referred to category %s, which does not exist\n", proj.Name(), name)
		} else {
			return fmt.Errorf("%s: can't fetch category %s: %v", proj.Name(), name, err)
		}
	}

	for _, name := range add {
		cat, err := db.FetchCategory(name)
		if err == nil {
			i := sort.SearchStrings(cat.Projects, proj.Name())
			if i < len(cat.Projects) && cat.Projects[i] == proj.Name() {
				//log.Printf("info: category %s already refers to project %s", name, proj.Name())
			} else {
				if i == len(cat.Projects) {
					cat.Projects = append(cat.Projects, proj.Name())
				} else {
					cat.Projects = append(cat.Projects[:i+1], cat.Projects[i:]...)
					cat.Projects[i] = proj.Name()
				}
				err = db.StoreCategory(cat, true)
				if err != nil {
					return fmt.Errorf("error storing category %s: %v", name, err)
				}
			}
		} else if errors.Is(err, gdbm.ErrItemNotFound) {
			log.Printf("warning: project %s refers to category %s, which does not exist\n", proj.Name(), name)
			//FIXME: remove erroneous entry?
		} else {
			return fmt.Errorf("%s: can't fetch category %s: %v", proj.Name(), name, err)
		}
	}

	// Update related projects
	rem, add, _ = ArrayDiff(orig.Related, proj.Related)
	for _, name := range rem {
		if name == proj.Name() {
			continue
		}
		pobj, err := db.FetchProject(name)
		if err == nil {
			i := sort.SearchStrings(pobj.Related, proj.Name())
			if i < len(pobj.Related) && pobj.Related[i] == proj.Name() {
				pobj.Related = append(pobj.Related[:i], pobj.Related[i+1:]...)
				err = db.StoreProject(pobj, true)
				if err != nil {
					return fmt.Errorf("Error storing project %s: %v", pobj.Name(), err)
				}
			}
		} else if errors.Is(err, gdbm.ErrItemNotFound) {
			log.Printf("warning: project %s refers to project %s, which does not exist\n", proj.Name(), name)
			//FIXME: Remove erroneous entry?
		} else {
			return fmt.Errorf("%s: can't fetch project %s: %v", proj.Name(), name, err)
		}
	}

	for _, name := range add {
		if name == proj.Name() {
			continue
		}
		pobj, err := db.FetchProject(name)
		if err == nil {
			i := sort.SearchStrings(pobj.Related, proj.Name())
			if i < len(pobj.Related) && pobj.Related[i] == proj.Name() {
				//log.Printf("info: project %s already refers to project %s", name, proj.Name())
			} else {
				if i == len(pobj.Related) {
					pobj.Related = append(pobj.Related, proj.Name())
				} else {
					pobj.Related = append(pobj.Related[:i+1], pobj.Related[i:]...)
					pobj.Related[i] = proj.Name()
				}
				err = db.StoreProject(pobj, true)
				if err != nil {
					return fmt.Errorf("error storing project %s: %v", name, err)
				}
			}
		} else if errors.Is(err, gdbm.ErrItemNotFound) {
			log.Printf("warning: project %s adds reference to project %s, which does not exist\n", proj.Name(), name)
			//FIXME: Remove erroneous entry?
		} else {
			return fmt.Errorf("%s: can't fetch project %s: %v", proj.Name(), name, err)
		}
	}

	return nil
}

func (db *Database) StoreProject(proj *project.Object, replace bool) (err error) {
	err = db.StoreObject(ProjectsType, proj, replace)
	if err != nil {
		db.lastErr = err
	}
	return
}

// func (db *Database) UpdateProject(proj *project.Object) (err error) {
//	err = db.StoreObject(ProjectsType, proj, true)
//	if err != nil {
//		db.lastErr = err
//		return fmt.Errorf("can't store project %s: %v", proj.Name(), err)
//	}
//	return db.IndexProject(proj)
// }

func (db *Database) FetchCategory(key string) (*category.Object, error) {
	obj, err := db.FetchObject(CategoryType, key)
	if err != nil {
		db.lastErr = err
		return nil, err
	}
	return obj.(*category.Object), nil
}

func (db *Database) StoreCategory(cat *category.Object, replace bool) (err error) {
	err = db.StoreObject(CategoryType, cat, replace)
	if err != nil {
		db.lastErr = err
	}
	return
}

// FIXME: lastErr?
func (db *Database) IndexCategory(cat, orig *category.Object) error {
	rem, add, _ := ArrayDiff(orig.Projects, cat.Projects)
	for _, name := range rem {
		proj, err := db.FetchProject(name)
		if err == nil {
			i := sort.SearchStrings(proj.Category, cat.Name())
			if i < len(proj.Category) && proj.Category[i] == cat.Name() {
				proj.Category = append(proj.Category[:i], proj.Category[i+1:]...)
				err = db.StoreProject(proj, true)
				if err != nil {
					return fmt.Errorf("Error storing project %s: %v", proj.Name(), err)
				}
			}
		} else if errors.Is(err, gdbm.ErrItemNotFound) {
			//log.Printf("warning: category %s referred to project %s, which does not exist\n", cat.Name(), name)
		} else {
			return fmt.Errorf("%s: can't fetch project %s: %v", cat.Name(), name, err)
		}
	}

	for _, name := range add {
		proj, err := db.FetchProject(name)
		if err == nil {
			i := sort.SearchStrings(proj.Category, cat.Name())
			if i < len(proj.Category) && proj.Category[i] == cat.Name() {
				//log.Printf("info: project %s already refers to category %s", name, cat.Name())
			} else {
				if i == len(proj.Category) {
					proj.Category = append(proj.Category, cat.Name())
				} else {
					proj.Category = append(proj.Category[:i+1], proj.Category[i:]...)
					proj.Category[i] = cat.Name()
				}
				err = db.StoreProject(proj, true)
				if err != nil {
					return fmt.Errorf("error storing project %s: %v", name, err)
				}
			}
		} else if errors.Is(err, gdbm.ErrItemNotFound) {
			log.Printf("warning: category %s added reference to project %s, which does not exist\n", cat.Name(), name)
			//FIXME: remove erroneous entry?
		} else {
			return fmt.Errorf("%s: can't fetch project %s: %v", cat.Name(), name, err)
		}
	}

	return nil
}

func (db *Database) Reindex() error {
	for i, _ := range db.relation {
		next := db.Iterator(i)
		var key string
		var err error
		for key, err = next(); err == nil; key, err = next() {
			obj, err := db.FetchObject(i, key)
			if err != nil {
				return fmt.Errorf("can't fetch %s %s: %v", RelDefs[i].id, key, err)
			}
			err = db.IndexObject(i, obj, db.NewObject(i, key))
			if err != nil {
				return err
			}
		}
		if !errors.Is(err, gdbm.ErrItemNotFound) {
			return fmt.Errorf("iteration finished abnormally: %v", err)
		}
	}
	return nil
}
