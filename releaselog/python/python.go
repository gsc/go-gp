package python

import (
	"regexp"
	"strings"
	"gp/releaselog"
	"gp/releaselog/common"	
)

var FormatName = `Python`
var LogName = `CHANGES.txt`

type LogParser struct {
	*common.LogParser
}

func NewLogParser() releaselog.LogParser {
	parser := LogParser{}
	parser.LogParser = common.NewLogParser(releaselog.LogHeader{
		// Release markers used by Python modules.
		{
			Rx: regexp.MustCompile(`^[vV]?(?P<version>(?:\d+\.)+\d+)\s*,\s*(?P<date>.*?)\s+-+\s*(?P<descr>.*)$`),
		        Lines: 1,
		},
		{
			Rx: regexp.MustCompile(`^[Vv]?(?P<version>(?:\d+\.)+\d+)\s*\((?P<date>.*)\)$`),
			Lines: 1,
		},
		{
			Rx: regexp.MustCompile(`^[vV]?(?P<version>(?:\d+\.)+\d+)\s+(?P<date>.*)$`),
			Lines: 1,
		},
	})
	return parser
}

func (p LogParser) IsEOE(scanner *releaselog.LineScanner) bool {
	if p.LogParser.NextLog != nil {
		return true
	} else if rlog := p.LogParser.HeaderTypes.Parse(scanner); rlog != nil {
		p.NextLog = rlog
		return true
	}
	return false
}

func (p LogParser) Line(s string) string {
	return strings.TrimLeft(s, " \t")
}

func init() { //nolint:gochecknoinits
	releaselog.RegisterFormat(FormatName, LogName, NewLogParser)
}
