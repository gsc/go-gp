package gnu

import (
	"regexp"
	"gp/releaselog"
	"gp/releaselog/common"
)

var FormatName = `GNU`
var LogName = `NEWS`

func NewLogParser() releaselog.LogParser {
	parser := common.NewLogParser(releaselog.LogHeader{
		// Traditional GNU NEWS format.
		{
			Rx: regexp.MustCompile(`^(?:\*\s+)?(?:(?i)version)\s+(?P<version>\d(?:[.,]\d+){1,2}(?:[\d._-])*)(?:.*[-,:]\s+(?P<date>.+))`),
			Lines: 1,
		},
		// Modification of GNU format used by GNU inetutils.
		{
			Rx: regexp.MustCompile(`(?m)^(?P<date>(?:A(?:pr(?:il)?|ug(?:ust)?)|Dec(?:ember)?|Feb(?:ruary)?|J(?:an(?:uary)?|u(?:ly?|ne?))|Ma(?:r(?:ch)?|y)|Nov(?:ember)?|Oct(?:ober)?|Sep(?:tember)?)\s+\d{1,2},\s+[12]\d{3})
Version\s+(?P<version>\d(?:[.,]\d+){1,2})`),
			Lines: 2,
		},
	})
	return parser
}

func init() { //nolint:gochecknoinits
	releaselog.RegisterFormat(FormatName, LogName, NewLogParser)
}
