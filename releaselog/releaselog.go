package releaselog

import (
	"regexp"
	"bufio"
	"io"
	"strings"
	"github.com/araddon/dateparse"
)

type ReleaseLog struct {      // Release log entry.
	Version string        // Version.
	TimeStamp string      // Release date/time.
	Description []string  // Release description (changes).
}

type HeaderType struct {      // Type of a release log header.
	Rx *regexp.Regexp     // Regular expression that the lines of this type must match.
	Lines int             // Number of lines matched by the regular expression.
}

// LineScanner is a modification of bufio.Scanner that scans input line by line and caches
// lines read.  This makes it possible to "put back" unused lines to stream.
type LineScanner struct {
	*bufio.Scanner
	cache []string
	Keep bool
}

// Create new line scanner.
func NewLineScanner(r io.Reader) *LineScanner {
	ls := &LineScanner{}
	ls.Scanner = bufio.NewScanner(r)
	return ls
}

// Scan next line.  Return true on success and false on end of file.
func (s *LineScanner) Scan() (ok bool) {
	if s.Len() > 0 {
		if s.Keep {
			s.Keep = false
			return true
		}
		s.cache = s.cache[1:]
	}
	ok = s.Len() > 0
	if !ok {
		ok = s.More()
	}
	return
}

func (s *LineScanner) PutBack() {
	s.Keep = true
}

// Read and cache one more line.  Return true on success and false on end of file.
func (s *LineScanner) More() (ok bool) {
	ok = s.Scanner.Scan()
	if ok {
		s.cache = append(s.cache, strings.TrimRight(s.Scanner.Text(), " \t"))
	}
	return
}

// Return number of cached lines.
func (s *LineScanner) Len() int {
	return len(s.cache)
}

// Return first cached line.
func (s *LineScanner) Head() string {
	return s.cache[0]
}

// Return N cached lines delimited by newlines.
func (s *LineScanner) Text(n int) (result string, ok bool) {
	ok = true
	for n > s.Len() {
		if ! s.More() {
			n = s.Len()
			ok = false
			break
		}
	}
	result = strings.Join(s.cache[0:n], "\n")
	return
}

// Skip next N input lines.
func (s *LineScanner) Skip(n int) {
	if n <= s.Len() {
		s.cache = s.cache[n:]
	} else {
		n -= s.Len()
		s.cache = []string{}
		for n > 0 {
			if !s.Scanner.Scan() {
				break
			}
		}
	}
}

type LogHeader []HeaderType

func (h LogHeader) Parse(scanner *LineScanner) (result *ReleaseLog) {
	for _, lh := range h {
		text, ok := scanner.Text(lh.Lines)
		if !ok {
			break
		}
		match := lh.Rx.FindStringSubmatch(text)
		if match != nil {
			ts := match[lh.Rx.SubexpIndex(`date`)]
			if _, err := dateparse.ParseAny(ts); err == nil {
				i := lh.Rx.SubexpIndex(`descr`)
				descr := []string{}
				if i != -1 {
					descr = []string{match[i]}
				}
				result = &ReleaseLog{
					Version: match[lh.Rx.SubexpIndex(`version`)],
					TimeStamp: ts,
					Description: descr,
				}
				scanner.Skip(lh.Lines)
				break
			}
		}
	}
	return
}

type LogParser interface {
	TestHeader(scanner *LineScanner) bool
	ParseHeader(scanner *LineScanner) *ReleaseLog
	Parser(scanner *LineScanner, limit int) []*ReleaseLog
	IsEOE(scanner *LineScanner) bool
	Line(string) string
	More() bool
}	

var EOE = regexp.MustCompile("\f+")

func (scanner *LineScanner) IsEOE() bool {
	return EOE.Match([]byte(scanner.Head()))
}

type NewLogParserFunc func () LogParser

type LogFormat struct {
	Name string
	LogName string
	NewFunc NewLogParserFunc
}

var logFormats []LogFormat

func RegisterFormat(name, logname string, newfunc NewLogParserFunc) {
	logFormats = append(logFormats, LogFormat{
		Name: name,
		LogName: logname,
		NewFunc: newfunc,
	})
}

func NewLogParser(logtype string) (LogParser, string) {
	for _, lf := range logFormats {
		if lf.Name == logtype {
			return lf.NewFunc(), lf.LogName
		}
	}
	return nil, ""
}

func LogName(logtype string) string {
	for _, lf := range logFormats {
		if lf.Name == logtype {
			return lf.LogName
		}
	}
	return ""
}

func Parser(r io.Reader, logtype string, limit int) []*ReleaseLog {
	scanner := NewLineScanner(r)
	var parser LogParser
	if logtype == `any` {
		var parsers []LogParser
		for _, lf := range logFormats {
			parsers = append(parsers, lf.NewFunc())
		}
	scanning:
		for scanner.Scan() {
			for _, p := range parsers {
				if p.TestHeader(scanner) {
					parser = p
					break scanning
				}
			}
		}
	} else {
		parser, _ = NewLogParser(logtype)
	}

	if parser == nil {
		return nil
	}
	return parser.Parser(scanner, limit)
}
