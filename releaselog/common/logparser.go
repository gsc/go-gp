package common

import (
	"gp/releaselog"
)

type LogParser struct {
	HeaderTypes releaselog.LogHeader
	NextLog *releaselog.ReleaseLog
}

func (p *LogParser) Line(s string) string {
	return s
}

func (p *LogParser) TestHeader(scanner *releaselog.LineScanner) bool {
	if rlog := p.HeaderTypes.Parse(scanner); rlog != nil {
		p.NextLog = rlog
		scanner.PutBack()
		return true
	}
	return false
}

func (p *LogParser) ParseHeader(scanner *releaselog.LineScanner) (rlog *releaselog.ReleaseLog) {
	if p.More() {
		rlog = p.NextLog
		p.NextLog = nil
		scanner.PutBack()
	} else {
		rlog = p.HeaderTypes.Parse(scanner)
		if rlog != nil {
			for scanner.Scan() {
				if scanner.Head() != "" {
					scanner.PutBack()
					break
				}
			}
		}
	}
	return
}

func (p *LogParser) More() bool {
	return p.NextLog != nil
}

func (p *LogParser) IsEOE(scanner *releaselog.LineScanner) bool {
	if p.More() {
		return true
	} else if rlog := p.HeaderTypes.Parse(scanner); rlog != nil {
		p.NextLog = rlog
		return true
	}
		
	return scanner.IsEOE()
}

func (lp *LogParser) Parser(scanner *releaselog.LineScanner, limit int) []*releaselog.ReleaseLog {
	const (
		InitialState = iota
		DescriptionState
		ParaState
	)

	var result []*releaselog.ReleaseLog
	var curlog *releaselog.ReleaseLog
	state := InitialState
scanning:
	for scanner.Scan() || (state == InitialState && lp.More()){
		switch state {
		case InitialState:
			rlog := lp.ParseHeader(scanner)
			if rlog != nil {
				if limit != 0 && len(result) == limit {
					break
				}
				curlog = rlog
				result = append(result, rlog)
				state = DescriptionState
			}
		case DescriptionState:
			if lp.IsEOE(scanner) {
				curlog = nil
				state = InitialState
			} else if scanner.Head() == "" {
				state = ParaState
			} else {
				curlog.Description = append(curlog.Description, lp.Line(scanner.Head()))
			}
		case ParaState:
			if lp.IsEOE(scanner) {
				curlog = nil
				state = InitialState
			} else if rlog := lp.ParseHeader(scanner); rlog != nil {
				if limit != 0 && len(result) == limit {
					break scanning
				}
				curlog = rlog
				result = append(result, rlog)
				state = DescriptionState
			} else if scanner.Head() != "" {
				curlog.Description = append(curlog.Description, "", lp.Line(scanner.Head()))
				state = DescriptionState
			}
		}
	}
	return result
}

func NewLogParser(htypes releaselog.LogHeader) *LogParser {
	return &LogParser{HeaderTypes: htypes}
}
