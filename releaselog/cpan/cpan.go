package cpan

import (
	"regexp"
	"gp/releaselog"
	"gp/releaselog/common"
)

var FormatName = `CPAN`
var LogName = `Changes`

func NewLogParser() releaselog.LogParser {
	parser := common.NewLogParser(releaselog.LogHeader{
		{
			Rx: regexp.MustCompile(`^(?P<version>(?:\d+\.)+\d+)\s+(?P<date>.+?)\s*$`),
			Lines: 1,
		},
	})
	return parser
}

func init() { //nolint:gochecknoinits
	releaselog.RegisterFormat(FormatName, LogName, NewLogParser)
}


