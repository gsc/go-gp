module gp

go 1.16

require (
	github.com/araddon/dateparse v0.0.0-20210429162001-6b43995a97de
	github.com/felixge/httpsnoop v1.0.2
	github.com/gomarkdown/markdown v0.0.0-20220419181919-412bcf14cd2e
	github.com/graygnuorg/go-gdbm v0.0.0-20220619212953-9461cbce407a
	github.com/lestrrat-go/strftime v1.0.6
	github.com/mohae/deepcopy v0.0.0-20170929034955-c48cc78d4826
	github.com/pborman/getopt/v2 v2.1.0
	gopkg.in/yaml.v2 v2.4.0
)
