package main

import (
	"os"
	"os/signal"
	"syscall"
	"fmt"
	"log"
	"net/http"
	"path/filepath"
	"html/template"
	"io/ioutil"
	"gp/object"
	"errors"
	"encoding/json"
	"github.com/graygnuorg/go-gdbm"
	"io"
	"sort"
	"net/url"
	"github.com/felixge/httpsnoop"
	"github.com/gomarkdown/markdown"
	"mime"
	"time"
	"strings"
	"strconv"
	"regexp"
)

func ServerAction(args []string) {
	optset := NewGPOptSet(args)
	optset.SetParameters("[ADDRESS]")
	optset.Parse()
	args = optset.Args()

	addr := Config.Address

	switch len(args) {
	case 0:
		// Nothing
	case 1:
		addr = args[0]
	default:
		log.Fatalln("bad arguments")
	}

	mux := &http.ServeMux{}
	mux.HandleFunc("/", staticHandler)

	if ! strings.HasSuffix(Config.ProjectListURI, `/`) {
		Config.ProjectListURI += `/`
	}
	mux.HandleFunc(Config.ProjectListURI,
		func (w http.ResponseWriter, r *http.Request) {
			project := strings.TrimPrefix(r.URL.Path, Config.ProjectListURI)
			if project == `` {
				projectListHandler(w, r, `projectlist.tmpl`, &LayoutData{Title: "Project List"})
			} else {
				projectHandler(w, r, `project.tmpl`, project)
			}
		})
	mux.HandleFunc(Config.ProjectDirURI,
		func (w http.ResponseWriter, r *http.Request) {
			projectListHandler(w, r, `dir.tmpl`, &LayoutData{Title: "Project Directory"})
		})
	if ! strings.HasSuffix(Config.CategoryListURI, `/`) {
		Config.CategoryListURI += `/`
	}
	mux.HandleFunc(Config.CategoryListURI,
		func (w http.ResponseWriter, r *http.Request) {
			catname := strings.TrimPrefix(r.URL.Path, Config.CategoryListURI)
			if catname == `` {
				listHandler(w, r, CategoryType, `catlist.tmpl`, &LayoutData{Title: "Software Categories"})
			} else {
				projectListHandler(w, r, `catprojects.tmpl`, &LayoutData{Title: catname, Arg: catname})
			}
		})

	hl := NewHTTPLogger()
	err := hl.OpenDefault()
	if err != nil {
		log.Println(err)
		hl.Open("-")
	}

	s := &http.Server{
		Addr: addr,
		Handler: http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			req_url := *r.URL
			if req_url.Host == "" {
				req_url.Host = r.Host
			}
			if req_url.Scheme == "" {
				h := strings.ToLower(r.Header.Get("X-Forwarded-Proto"))
				switch h {
				case `http`,`https`:
					req_url.Scheme = h

				default:
					req_url.Scheme = `http`
				}
			}
			fn := func (w http.ResponseWriter) {
				mux.ServeHTTP(w, r)
			}
			if path, status, found := Config.RewritePath(r.URL.Path); found {
				url, err := url.Parse(path)
				if err != nil {
					log.Printf("can't parse URL %s", path)
					fn = func (w http.ResponseWriter) {
						errorHandler(w, r, http.StatusInternalServerError)
					}
				} else {
					r.URL = req_url.ResolveReference(url)
					if Config.RewriteDebug {
						log.Printf("new URL: %v", r.URL)
					}
					if status != 0 {
						fn = func (w http.ResponseWriter) {
							w.Header().Set("Location", r.URL.String())
							w.WriteHeader(status)
						}
					}
				}
			}
			m := httpsnoop.CaptureMetricsFn(w, fn)
			hl.Format(m, r)
		}),
	}

	signal_chan := make(chan os.Signal, 1)
	signal.Notify(signal_chan, syscall.SIGHUP, syscall.SIGTERM)
	go func() {
		log.Fatal(s.ListenAndServe())
	}()

sigloop:
	for {
		msg := <-signal_chan
		switch msg {
		case syscall.SIGHUP:
			err = hl.OpenDefault()
			if err != nil {
				log.Println(err)
				hl.Open("-")
			}

		default:
			hl.Close()
			break sigloop
		}
	}
}

func nameFunc(x interface{}) (string, error) {
	if obj, ok := x.(object.Object); ok {
		return obj.Name(), nil
	}
	return "", errors.New("not an object")
}

func jsonFunc(obj interface{}) (result string, err error) {
	var b []byte
	b, err = json.MarshalIndent(obj, "", "\t")
	if err == nil {
		result = string(b)
	}
	return
}

type LayoutData struct {
	Title string
	Arg interface{}
}

func MergeMaps(a, b template.FuncMap) template.FuncMap {
	res := template.FuncMap{}
	for k, v := range a {
		res[k] = v
	}
	for k, v := range b {
		res[k] = v
	}
	return res
}

func WildPatternToRegexp(pattern string) (*regexp.Regexp, error) {
	expr := []byte(`^(?i)`)

	for pattern != "" {
		i := strings.IndexAny(pattern, "*?[")
		if i == -1 {
			expr = append(expr, []byte(regexp.QuoteMeta(pattern))...)
			break
		} else {
			expr = append(expr, []byte(regexp.QuoteMeta(pattern[0:i]))...)
			switch pattern[i] {
			case '*':
				expr = append(expr, []byte(".*")...)
			case '?':
				expr = append(expr, byte('.'))
			case '[':
				k := 0
				if pattern[i+1] == ']' {
					k = 1
				}
				j := strings.IndexByte(pattern[i+k+1:], byte(']'))
				if j == -1 {
					expr = append(expr, []byte(regexp.QuoteMeta(pattern[i:i+k+1]))...)
					i += k
				} else {
					expr = append(expr, []byte(pattern[i:i+k+j+2])...)
					i += k + j + 1
				}
			}
			pattern = pattern[i+1:]
		}
	}

	return regexp.Compile(string(expr))
}

func WildMatch(pattern string, a []string) []string {
	rx, err := WildPatternToRegexp(pattern)
	var match func (string) bool
	if err == nil {
		match = func (s string) bool { return rx.FindStringIndex(s) != nil }
	} else {
		match = func (s string) bool { return s == pattern }
	}
	ret := []string{}
	for _, s := range a {
		if match(s) {
			ret = append(ret, s)
		}
	}
	return ret
}

func CreateTemplate(filename string, db *Database, r *http.Request) (*template.Template, error) {
	templfile := filepath.Join(Config.RootDir, Config.TemplateDir, `default.tmpl`)
	content, err := ioutil.ReadFile(templfile)
	if err != nil {
		return nil, err
	}

	defaultFuncMap := template.FuncMap{
		"odd":  func (n int) bool {
				return n % 2 != 0
			},
		"even": func (n int) bool {
				return n % 2 == 0
			},
		"decr": func (n int) int {
				return n - 1
			},
		"incr": func (n int) int {
				return n + 1
			},
		"ThisURL":func () *url.URL {
				return r.URL
			},
		"query": func (name string) string {
				return r.URL.Query().Get(name)
			},
		"clearquery": func (names ...string) *url.URL {
				  u, err := url.Parse(r.URL.String())
				  if err != nil {
					  log.Printf("can't clone URL %s: %v", r.URL.String(), err)
					  return r.URL
				  }
				  q := u.Query()
				  for _, name := range names {
					  q.Del(name)
				  }
				  u.RawQuery = q.Encode()
				  return u
			},
		"sort": func (s []string) []string {
				sort.Strings(s)
				return s
			},
		"Config": func () *GPConfig {
				return &Config
			},
		"markdown":func (text string) template.HTML {
			return template.HTML(markdown.ToHTML([]byte(text), nil, nil))
		},
		"now": func () time.Time { return time.Now(); },
	}
	var top *template.Template
	top, err = template.New("default").Funcs(defaultFuncMap).Parse(string(content))
	if err != nil {
		return nil, err
	}

	templfile = filepath.Join(Config.RootDir, Config.TemplateDir, filename)
	content, err = ioutil.ReadFile(templfile)
	if err != nil {
		return nil, err
	}
	_, err = top.New("content").Funcs(MergeMaps(defaultFuncMap, template.FuncMap{
		"json": jsonFunc,
		"name": nameFunc,
		"get":  func (what, key string) (obj object.Object, err error) {
			        if rel, found := FindRelation(what); found {
					obj, err = db.FetchObject(rel, key)
				} else {
					err = fmt.Errorf("unknown relation: %s", what)
				}
			        return
		        },
		"project": func (key string) (object.Object, error) {
			        return db.FetchProject(key)
		           },
		"category": func (key string) (object.Object, error) {
			        return db.FetchCategory(key)
		           },
		"names":func (what string, args ...string) (result []string, err error) {
			    if rel, found := FindRelation(what); found {
			            sortby := "n"
				    sortord := "a"
				    switch len(args) {
				    case 2:
				            sortord = args[1]
				            fallthrough
			            case 1:
				            if args[0] != "" {
					            sortby = args[0]
				            }
			            case 0:
					    // ok
				    default:
				            err = errors.New("too many argments passed to names")
			            }
			            result, err = db.SortedKeys(rel, sortby, sortord)
			            if err != nil {
				            log.Println(err)
					    err = nil // This error is not fatal
			            }
			    } else {
				    err = fmt.Errorf("unknown relation: %s", what)
			    }

			    return
			},
		"wildmatch": WildMatch,
		},
	)).Parse(string(content))
	if err != nil {
		return nil, err
	}
	return top, err
}

type TextBuffer struct {
	data []byte
}

func (t *TextBuffer) Write(s []byte) (n int, err error) {
	t.data = append(t.data, s...)
	n = len(s)
	return
}

func (t *TextBuffer) Flush(w io.Writer) error {
	_, err := io.WriteString(w, string(t.data))
	t.data = []byte{}
	return err
}

func projectListHandler(w http.ResponseWriter, r *http.Request, templateName string, layout *LayoutData) {
	listHandler(w, r, ProjectsType, templateName, layout)
}

func projectHandler(w http.ResponseWriter, r *http.Request, templateName string, project string) {
	listHandler(w, r, ProjectsType, templateName, &LayoutData{Title: project, Arg: project})
}

func listHandler(w http.ResponseWriter, r *http.Request, dbtype int, templateName string, layout *LayoutData) {
	db, err := OpenDatabase(gdbm.ModeReader)
	if err != nil {
		log.Printf("Can't open database: %v", err)
		errorHandler(w, r, http.StatusInternalServerError)
		return
	}
	defer db.Close()

	t, err := CreateTemplate(templateName, db, r)
	if err != nil {
		log.Printf("Creating template from %s: %v", templateName, err)
		errorHandler(w, r, http.StatusInternalServerError)
		return
	}

	ts := &TextBuffer{}
	err = t.Execute(ts, &layout)

	if err != nil {
		log.Printf("Error executing template: %v", err)
		status := http.StatusInternalServerError
		if errors.Is(db.LastError(), gdbm.ErrItemNotFound) {
			status = http.StatusNotFound
		}
		errorHandler(w, r, status)
		return
	}

	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	ts.Flush(w)
	return
}

func staticHandler(w http.ResponseWriter, r *http.Request) {
	filename := filepath.Join(Config.RootDir, Config.StaticDir, r.URL.Path)
	st, err := os.Stat(filename)
	if err != nil {
		log.Printf("/%s: %v\n", filename, err)
		errorHandler(w, r, http.StatusNotFound)
		return
	}
	if st.IsDir() {
		errorHandler(w, r, http.StatusNotFound)
		return
	}

	filetype := mime.TypeByExtension(filepath.Ext(filename))
	if filetype == "" {
		filetype = `application/octet-stream`
	}

	file, err := os.Open(filename)
	if err != nil {
		log.Printf("/%s: %v\n", filename, err)
		errorHandler(w, r, http.StatusServiceUnavailable)
		return
	}
	defer file.Close()

	w.Header().Set("Content-Type", filetype)
	io.Copy(w, file)
}

func errorHandler(w http.ResponseWriter, r *http.Request, status int) {
	w.Header().Set("Content-Type", "text/html")
	w.WriteHeader(status)

	filename := filepath.Join(Config.RootDir, Config.ErrorDir, strconv.Itoa(status) + `.html`)
	file, err := os.Open(filename)

	if err != nil {
		fmt.Fprintf(w, `<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
<html>
  <head>
    <title>%d %s</title>
  </head>
  <body>
    <h1>%s</h1>
    <p></p>
  </body>
</html>`,
			status, http.StatusText(status), http.StatusText(status))
	} else {
		io.Copy(w, file)
		file.Close()
	}
}
