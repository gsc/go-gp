package object

import (
	"github.com/graygnuorg/go-gdbm"
	"text/template"
	"encoding/json"
	"gopkg.in/yaml.v2"
	"errors"
	"fmt"
	"os"
	"io"
	"sort"
	"time"
	"strings"
	"reflect"
	"regexp"
	"syscall"
	"log"
	"github.com/mohae/deepcopy"
)

var (
	ErrNotAnObject = errors.New("not an object")
)

type Object interface {
	Name() string
	SetName(s string)
	Init()
	Time() (time.Time, error)
	Normalize()
}

type Base struct {
	name string
}

func (p *Base) Name() string {
	return p.name
}

func (p *Base) SetName(s string) {
	p.name = s
}

type Database struct {
	*gdbm.Database
	entrytype reflect.Type
	backup string
}

func IsNotSupported(err error) bool {
	if errors.Is(err, gdbm.ErrNotImplemented) {
		return true
	}
	if gerr, ok := err.(*gdbm.GdbmError); ok {
		return gerr.SysError() == syscall.ENOTSUP
	}
	return false
}

func BackupName(name string) string {
	return name + `.bak`
}

// Open database for updating.  Prefer crash tolerance mode so that eventual readers
// can use one of the snapshots instead of the locked database.  If crash tolerance
// is not available, create a read-only copy of the database for use by readers.  In
// that case return the name of the created copy along with the database and error.
func OpenRW(name string, mode int) (gd *gdbm.Database, backup string, err error) {
	gd, err = gdbm.OpenConfig(gdbm.DatabaseConfig{FileName: name,
						      CrashTolerance: true,
						      Mode: mode,
						      FileMode: 0666,
						      Flags: gdbm.OF_NUMSYNC})

	switch {
	case err == nil:
		//Ok

	case errors.Is(err, gdbm.ErrSnapshotExist):
		// Database was not closed properly.  Attempt a crash recovery:
		err = gdbm.SnapshotRestore(name)
		if err != nil {
			err = fmt.Errorf("can't restore %s: %w; manual crash recovery advised", name, err)
			return
		}
		// Database was successfully recovered, now try to open it:
		gd, err = gdbm.OpenConfig(gdbm.DatabaseConfig{FileName: name,
							      CrashTolerance: true,
							      Mode: mode,
							      FileMode: 0666,
							      Flags: gdbm.OF_NUMSYNC})
		if err != nil {
			err = fmt.Errorf("can't open %s after crash recovery: %w", name, err)
		}

	// case errors.Is(err, gdbm.ErrNotImplemented) || (errors.Is(err, gdbm.ErrSnapshotClone):
	case IsNotSupported(err):
		gd, err = gdbm.Open(name, mode)
		if err != nil {
			return
		}

		// Create a read-only copy of the database
		backup = BackupName(name)
		src, err := os.Open(name)
		if err == nil {
			defer src.Close()

			umask := syscall.Umask(0366)
			dst, err := os.Create(backup)
			syscall.Umask(umask)
			if err == nil {
				defer dst.Close()

				if _, err = io.Copy(dst, src); err != nil {
					os.Remove(backup)
				}
			}
		}
	}
	return
}

// Open database for reading.  If the database is locked for writing, try to use one of its
// snapshots.  If snapshots are not available, try to use the backup copy.
func OpenRO(name string) (gd *gdbm.Database, err error) {
	gd, err = gdbm.Open(name, gdbm.ModeReader)
	switch {
	case err == nil:
		return

	case errors.Is(err, gdbm.ErrCantBeReader):
		var snapname string
		snapshots := gdbm.SnapshotNames(name)
		snapname, err = snapshots.Select()
		if err != nil {
			snapname = BackupName(name)
		}

		gd, err = gdbm.Open(snapname, gdbm.ModeReader)
		if err != nil {
			err = fmt.Errorf("can't open database %s: %w; additionally, opening snapshot/backup %s failed with %w", name, gdbm.ErrCantBeReader, snapname, err)
		}
	}
	return
}

func Open(name string, mode int, t Object) (db *Database, err error) {
	var (
		gd *gdbm.Database
		backup string
	)
	if mode == gdbm.ModeReader {
		gd, err = OpenRO(name)
	} else {
		gd, backup, err = OpenRW(name, mode)
	}
	if err == nil {
		db = &Database{Database: gd, entrytype: reflect.TypeOf(t), backup: backup}
	}
	return
}

func (db *Database) Close() {
	err := db.Database.Close()
	if err != nil {
		log.Println("Can't close database: ", err)
	}
	if db.backup != "" {
		os.Remove(db.backup)
		db.backup = ""
	}
}

type DatabaseIterator func () (string, error)

func (db *Database) StringIterator() DatabaseIterator {
	next := db.Iterator()
	return func() (res string, err error) {
		var b []byte
		b, err = next()
		if err == nil {
			res = string(b)
		}
		return
	}
}

func (db *Database) Unmarshal(name string, jv []byte) (value Object, err error) {
	v := reflect.New(db.entrytype.Elem())
	ptr := v.Interface()
	err = json.Unmarshal(jv, ptr)
	if err == nil {
		var ok bool
		value, ok = ptr.(Object)
		if ok {
			value.SetName(name)
		} else {
			err = ErrNotAnObject
		}
	}
	return
}

func (db *Database) FetchObject(name string) (value Object, err error) {
	var jv []byte
	jv, err = db.Fetch([]byte(name))
	if err == nil {
		value, err = db.Unmarshal(name, jv)
	}
	return
}

func (db *Database) StoreObject(obj Object, replace bool) (err error) {
	obj.Normalize()
	data, err := json.Marshal(obj)
	if err == nil {
		err = db.Store([]byte(obj.Name()), data, replace)
	}
	return
}

func (db *Database) UpdateObject(name string, update []byte) (obj Object, orig Object, err error) {
	obj, err = db.FetchObject(name)
	if err == nil {
		orig = Clone(obj)
		err = json.Unmarshal(update, &obj)
		if err == nil {
			err = db.StoreObject(obj, true)
		}
	}
	return
}

func (db *Database) DeleteObject(name string) error {
	return db.Delete([]byte(name))
}

func nameFunc(obj Object) string {
	return obj.Name()
}

func jsonFunc(obj interface{}) (result string, err error) {
	var b []byte
	b, err = json.MarshalIndent(obj, "", "\t")
	if err == nil {
		result = string(b)
	}
	return
}

func yamlFunc(obj interface{}) (result string, err error) {
	var b []byte
	b, err = yaml.Marshal(obj)
	if err == nil {
		result = string(b)
//		if x, ok := obj.(Object); ok {
//			result = "Name: " + x.Name() + "\n" + result
//		}
	}
	return
}

func Template() *template.Template {
	return template.New("object").Funcs(template.FuncMap{
		"json": jsonFunc,
		"yaml": yamlFunc,
		"name": nameFunc,
		"nl": func () string { return "\n" },
	})
}

func ParseTemplate(format string) *template.Template {
	return template.Must(Template().Parse(format))
}

func (db *Database) List(tmpl *template.Template) error {
	next := db.StringIterator()
	var key string
	var err error
	for key, err = next(); err == nil; key, err = next() {
		obj, err := db.FetchObject(key)
		if err != nil {
			return err
		}
		err = tmpl.Execute(os.Stdout, obj)
		if err != nil {
			return err
		}
		fmt.Println("")
	}
	if !errors.Is(err, gdbm.ErrItemNotFound) {
		return err
	}
	return nil
}

func (db *Database) Dump(wr io.Writer) (err error) {
	next := db.StringIterator()
	var key string
	km := make(map[string]Object)
	for key, err = next(); err == nil; key, err = next() {
		var obj Object
		obj, err = db.FetchObject(key)
		if err != nil {
			return
		}
		km[obj.Name()] = obj
	}
	if !errors.Is(err, gdbm.ErrItemNotFound) {
		return fmt.Errorf("iteration finished abnormally: %v", err)
	}

	var b []byte
	b, err = json.MarshalIndent(km, "", "\t")
	if err != nil {
		return
	}
	wr.Write(b)
	return
}

func (db *Database) Keys() (result []string, err error) {
	next := db.StringIterator()
	var key string
	for key, err = next(); err == nil; key, err = next() {
		result = append(result, key)
	}
	if errors.Is(err, gdbm.ErrItemNotFound) {
		err = nil
	}
	return
}

func (db *Database) dateLess(a, b string) bool {
	var (
		ta time.Time
		tb time.Time
	)

	obj, err := db.FetchObject(a)
	if err != nil {
		return false
	}
	ta, err = obj.Time()

	obj, err = db.FetchObject(b)
	if err != nil {
		return false
	}
	tb, err = obj.Time()
	return ta.Before(tb)
}

func (db *Database) SortedKeys(sortby string, sortord string) (result []string, err error) {
	result, err = db.Keys()
	if err != nil {
		return
	}
	switch sortby + sortord {
	case ``, `n`, `na`: // Name Ascending
		sort.Slice(result, func (i, j int) bool { return strings.ToLower(result[i]) < strings.ToLower(result[j]) })
	case `nd`: // Name Descending
		sort.Slice(result, func (i, j int) bool { return strings.ToLower(result[j]) < strings.ToLower(result[i]) })
	case `d`, `da`: // Date Ascending
		sort.Slice(result, func (i, j int) bool { return db.dateLess(result[i], result[j]); })
	case `dd`: // Date Descending
		sort.Slice(result, func (i, j int) bool { return db.dateLess(result[j], result[i]); })
	default:
		err = errors.New("Unrecognized sort parameters: " + sortby + sortord)
	}
	return
}

func (db *Database) NewObject(name string) Object {
	v := reflect.New(db.entrytype.Elem())
	ptr := v.Interface()
	obj, ok := ptr.(Object)
	if ok {
		obj.SetName(name)
		obj.Init()
	} else {
		panic(ErrNotAnObject)
	}
	return obj
}

func findStringPrefix(a []string, p string) int {
	p += `:`
	for i, s := range a {
		if strings.HasPrefix(s, p) {
			return i
		}
	}
	return -1
}

var objRx = regexp.MustCompile(`^\*?(.+)\.Object`)

func Annotate(obj Object, wr io.Writer) error {
	b, err := yaml.Marshal(obj)
	if err != nil {
		return err
	}
	astr := strings.Split(string(b), "\n")
	astr = append(astr[:2], astr[0:]...)

	ts := reflect.TypeOf(obj).String()
	if match := objRx.FindStringSubmatch(ts); match != nil {
		ts = match[1]
	}
	astr[0] = `## ` + ts + ` ` + obj.Name()
	astr[1] = ``

	v := reflect.ValueOf(obj)
	if v.Kind() == reflect.Ptr {
		v = v.Elem()
	}
	if v.Kind() != reflect.Struct {
		return errors.New(fmt.Sprintf("Passed object has unsupported type: %s", v.Kind()))
	}
	t := v.Type()
	for i := 0; i < v.NumField(); i++ {
		f := t.Field(i)
		name := f.Tag.Get(`yaml`)
		if name == "" {
			continue
		}
		n := strings.IndexByte(name, byte(','))
		if n != -1 {
			name = name[0:n]
		}
		if name == "" || name == "-" {
			continue
		}
		d := f.Tag.Get(`rem`)
		j := findStringPrefix(astr, name)
		if j != -1 {
			astr = append(astr[:j+1], astr[j:]...)
			astr[j] = `# ` + d
		}
	}
	fmt.Fprintln(wr, strings.Join(astr, "\n"))
	return nil
}

func Clone(obj Object) Object {
	newobj := deepcopy.Copy(obj).(Object)
	newobj.SetName(obj.Name())
	return newobj
}
