package category

import (
	"gp/object"
	"errors"
	"time"
	"sort"
)

type Object struct {
	object.Base		`yaml:"-"`
	Summary string		`yaml:"Summary" rem:"Detailed summary of the category."`
	Description string	`yaml:"Description" rem:"Short one-line description of the category."`
	Projects []string       `yaml:"Projects" rem:"List of related projects"`
}

func (cat *Object) Time() (t time.Time, err error) {
	err = errors.New("Not implemented")
	return
}

func (cat *Object) Init() {
}

func (cat *Object) Normalize() {
	sort.Strings(cat.Projects)
}
